Rails.application.routes.draw do
  devise_for :users, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    registration: 'signup'
  },
  controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resources :books, only: [:index]

  resources :carts, only: [] do
    collection do
      post 'add_item'
      post 'remove_item'
      get 'show'
      post 'checkout'
    end
  end
end
