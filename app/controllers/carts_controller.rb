class CartsController < ApplicationController
  before_action :authenticate_request, :set_cart

  def add_item
    book = Book.find(params[:book_id])
    cart_item = @current_user.cart.cart_items.build(book: book, quantity: params[:quantity])
    if cart_item.save
      render json: @current_user.cart.cart_items, status: :ok
    else
      render json: { errors: cart_item.errors }, status: :unprocessable_entity
    end
  end

  def remove_item
    cart_item = @current_user.cart.cart_items.find_by(book_id: params[:book_id])
    if cart_item
      cart_item.destroy
      render json: @current_user.cart.cart_items, status: :ok
    else
      render json: { error: 'Item not found in cart' }, status: :not_found
    end
  end

  def show
    render json: { books: @current_user.cart.cart_items, total_price: "#{@current_user.cart.total_price}$"}
  end

  def checkout
    return render json: { error: 'Cart is empty' }, status: :unprocessable_entity if @current_user.cart.cart_items.empty?
    render json: checkout_dummy_response, status: :ok
    @current_user.cart.cart_items.destroy_all
  end

  private

  def authenticate_request
    @current_user = current_user
    render json: { error: 'Not Authorized' }, status: 401 unless @current_user
  end

  def set_cart
    @current_user.cart ||= Cart.create(user: @current_user)
  end

  def checkout_dummy_response
    {
      message: 'Payment successful',
      payment_method: 'Credit Card',
      payment_id: SecureRandom.uuid,
      total_price: @current_user.cart.total_price,
      currency: 'USD'
    }
  end
end
