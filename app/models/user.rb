class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher

  devise :database_authenticatable, :registerable, :recoverable,
          :jwt_authenticatable, jwt_revocation_strategy: self,
          authentication_keys: [:name]

  has_one :cart, dependent: :destroy

  validates :name, uniqueness: true, presence: true
  validates :password, format: { with: /\A(?=.*[a-zA-Z])(?=.*[0-9]).{6,}\z/ }
end
