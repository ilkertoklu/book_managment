# Book Management: API for Managing Books
All requirements are implemented and tested :white_check_mark: 
---
This API provides users to perform basic actions such as:
  - Signing up
  - Logging in
  - Logging out
  - Listing the existing books in store
  - Adding books to their cart
  - Removing books from their cart
  - Listing the books added to the cart
  - Purchasing the books added to the cart

The actions are protected with JWT token based authentication.

## Project Environment
  * Ruby 3.3.0
  * Ruby on Rails 7.1.3
  * RSpec 3.13.0
  * PostgreSQL 14.11
  * Docker 26.0.0
  * Docker Compose v2.26.1

### Gems
  * [devise](https://github.com/heartcombo/devise) - User authentication
  * [devise-jwt](https://github.com/waiting-for-dev/devise-jwt) - JWT token based authentication
  * [rspec-rails](https://github.com/rspec/rspec-rails) - Testing framework
  * [factory_bot_rails](https://github.com/thoughtbot/factory_bot_rails) - Test data generator
  * [faker](https://github.com/faker-ruby/faker) - Fake data generator used to seed the database

## Installation
### Clone this repository
```bash
git clone git@gitlab.com:ilkertoklu/book_managment.git
```
### Change directory and start the Docker containers
```bash
cd book_managment && docker-compose up -d
```

### Connect to the database
```bash
docker exec -it book_managment-db-1 psql -U postgres
```

### Create the user and grant the necessary permissions
```bash
CREATE USER iwt WITH PASSWORD 'foobar';
ALTER USER iwt CREATEDB;
```

### Create the database and run the migrations and seed the database
```bash
docker exec -it book_managment-rails-1 rails db:create db:migrate db:seed
```

API runs on `http://localhost:3000` and the database runs on `localhost:5432` by default. :rocket:

## API Endpoints
### Sign Up
```bash
POST /signup
```
:warning: Password must be at least 6 characters long and include at least one letter and one number.
#### Request
```json
{
    "user": {
        "name": "ilker",
        "password": "123456xyz"
    }
}
```
#### Response
```json
{
    "status": {
        "code": 200,
        "message": "Signed up successfully."
    },
    "data": {
        "id": 32,
        "name": "ilker"
    }
}
```
### Log In
```bash
POST /login
```
#### Request
```json
{
    "user": {
        "name": "ilker",
        "password": "123456xyz"
    }
}
```
#### Response
```json
{
    "status": {
        "code": 200,
        "message": "Logged in successfully.",
        "data": {
            "user": {
                "id": 32,
                "name": "ilker"
            }
        }
    }
}
```
Header includes the JWT token for the user to perform the actions. :key:

### Log Out
:warning: JWT token is required in request header.
```bash
DELETE /logout
```
#### Request Header
```json
{
    "Authorization ": "Bearer <JWT_TOKEN >"
}
```
#### Response
```json
{
    "status": {
        "code": 200,
        "message": "Logged out successfully."
    }
}
```

### List Books
```bash
GET /books
```
#### Response
```json
[
    {
        "id": 1,
        "title": "All Passion Spent",
        "author": "Lindy Jacobson",
        "price": "47.36",
        "created_at": "2024-05-12T15:08:58.347Z",
        "updated_at": "2024-05-12T15:08:58.347Z"
    },
    {
        "id": 2,
        "title": "Many Waters",
        "author": "Tomika Blanda",
        "price": "81.11",
        "created_at": "2024-05-12T15:08:59.105Z",
        "updated_at": "2024-05-12T15:08:59.105Z"
    }
    ...
]
```

### Add Book to Cart
:warning: JWT token is required in request header.
```bash
POST /carts/add_item
```
#### Request
```json
{
  {
      "book_id": 3,
      "quantity": 2
  }
}
```
#### Response
includes the cart item details. :shopping_cart:

```json
[
    {
        "id": 5,
        "cart_id": 31,
        "book_id": 3,
        "quantity": 2,
        "created_at": "2024-05-12T16:39:17.025Z",
        "updated_at": "2024-05-12T16:39:17.025Z"
    }
]
```

### Remove Book from Cart
:warning: JWT token is required in request header.
```bash
DELETE /carts/remove_item
```

### Request
```json
{
    "book_id": 3
}
```

#### Response
```json
[
    {
        "id": 6,
        "cart_id": 31,
        "book_id": 3,
        "quantity": 2,
        "created_at": "2024-05-12T16:39:38.767Z",
        "updated_at": "2024-05-12T16:39:38.767Z"
    },
    {
        "id": 7,
        "cart_id": 31,
        "book_id": 3,
        "quantity": 2,
        "created_at": "2024-05-12T16:39:40.860Z",
        "updated_at": "2024-05-12T16:39:40.860Z"
    }
]
```

### Show Cart
:warning: JWT token is required in request header.
```bash
GET /carts/show
```

#### Response
```json
{
    "books": [
        {
            "id": 6,
            "cart_id": 31,
            "book_id": 3,
            "quantity": 2,
            "created_at": "2024-05-12T16:39:38.767Z",
            "updated_at": "2024-05-12T16:39:38.767Z"
        },
        {
            "id": 7,
            "cart_id": 31,
            "book_id": 3,
            "quantity": 2,
            "created_at": "2024-05-12T16:39:40.860Z",
            "updated_at": "2024-05-12T16:39:40.860Z"
        }
    ],
    "total_price": "82.68$"
}
```

### Purchase Cart: Checkout
:warning: JWT token is required in request header.
```bash
POST /carts/checkout
```

#### Response
```json
{
    "message": "Payment successful",
    "payment_method": "Credit Card",
    "payment_id": "421322d9-05f3-43b8-b623-673f63f13ee9",
    "total_price": "82.68",
    "currency": "USD"
}
```

## Testing
RSpec is used for testing. Run the tests with the following command:
```bash
docker exec -it book_managment-rails-1 rspec # => 14 examples, 0 failures
```
