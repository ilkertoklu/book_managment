10.times do
  Book.create(
    title: Faker::Book.title,
    author: Faker::Book.author,
    price: Faker::Number.decimal(l_digits: 2)
  )

  3.times do
    user = User.create(
      name: Faker::Name.name,
      password: Faker::Internet.password(min_length: 6, max_length: 10, mix_case: true)
    )

    user.create_cart if user.cart.nil?
  end
end
