# spec/factories/books.rb
FactoryBot.define do
  factory :book do
    title { "Sample Book" }
    author { "Author Name" }
    price { 15.99 }
  end
end
