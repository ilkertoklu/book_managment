# spec/factories/users.rb
FactoryBot.define do
  factory :user do
    name { "ilker" }
    password { "password123" }

    after(:create) do |user|
      user.create_cart
    end
  end
end
