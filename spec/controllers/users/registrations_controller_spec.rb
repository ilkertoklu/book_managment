require 'rails_helper'

RSpec.describe 'Users::Registrations', type: :request do
  describe 'POST /signup' do
    context 'with valid parameters' do
      let(:valid_params) { { user: { name: 'foo', password: 'password123' } } }

      it 'creates a new user' do
        expect {
          post '/signup', params: valid_params, as: :json
        }.to change(User, :count).by(1)
      end

      it 'returns a successful response' do
        post '/signup', params: valid_params, as: :json
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)['status']['code']).to eq(200)
        expect(JSON.parse(response.body)['status']['message']).to eq('Signed up successfully.')
        expect(JSON.parse(response.body)['data']).to be_present
      end
    end

    context 'with invalid parameters' do
      let(:invalid_params) { { user: { name: 'invalid_name', password: 'short' } } }

      it 'does not create a new user' do
        expect {
          post '/signup', params: invalid_params, as: :json
        }.not_to change(User, :count)
      end

      it 'returns an unprocessable entity response' do
        post '/signup', params: invalid_params, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(JSON.parse(response.body)['status']['message']).to include("User couldn't be created successfully.")
      end
    end
  end
end
