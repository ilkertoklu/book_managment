require 'rails_helper'

RSpec.describe 'Users::Sessions', type: :request do
  let(:user) { create(:user, name: 'ilker', password: '123456xyz') }
  valid_user_token = ''

  describe 'POST /login' do
    context 'with valid credentials' do
      let(:valid_params) { { user: { name: user.name, password: user.password } } }

      it 'logs in the user' do
        post '/login', params: valid_params, as: :json
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)['status']['code']).to eq(200)
        expect(JSON.parse(response.body)['status']['message']).to eq('Logged in successfully.')
        expect(JSON.parse(response.body)['status']['data']['user']).to include('id' => user.id, 'name' => user.name)
        valid_user_token = response.headers['Authorization']
        expect(valid_user_token).to be_present
      end
    end

    context 'with invalid credentials' do
      let(:invalid_params) { { user: { name: user.name, password: 'wrongpassword' } } }

      it 'does not log in the user' do
        post '/login', params: invalid_params, as: :json
        expect(response).to have_http_status(:unauthorized)
        parsed_response = JSON.parse(response.body)
        expect(parsed_response).to eq({ "error" => "Invalid Name or password." })
      end
    end
  end

describe 'DELETE /logout' do
  before do
    post '/login', params: { user: { name: user.name, password: '123456xyz' } }, as: :json
    valid_user_token = response.headers['Authorization']
  end

  context 'with a valid session' do
    it 'logs out the user' do
      delete '/logout', headers: { 'Authorization' => valid_user_token }
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)['status']).to eq(200)
      expect(JSON.parse(response.body)['message']).to eq('Logged out successfully.')
    end
  end

  context 'without a valid session' do
    it 'returns an unauthorized response' do
      delete '/logout', headers: { 'Authorization' => 'Invalid token' }
      expect(response).to have_http_status(:unauthorized)
      expect(JSON.parse(response.body)['status']).to eq(401)
      expect(JSON.parse(response.body)['message']).to eq("Couldn't find an active session.")
    end
  end
end
end
