require 'rails_helper'

RSpec.describe BooksController, type: :controller do
  describe "GET #index" do
    before do
      FactoryBot.create_list(:book, 3, title: 'Sample Book', author: 'Author Name', price: 15.99)
    end

    it "returns all books with a success status" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "returns the correct number of books" do
      get :index
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(3)
    end

    it "returns books with correct data structure" do
      get :index
      json_response = JSON.parse(response.body)
      expect(json_response.first).to include("title", "author", "price")
    end
  end
end
