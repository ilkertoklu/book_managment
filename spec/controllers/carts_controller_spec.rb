require 'rails_helper'

RSpec.describe CartsController, type: :controller do
  let(:user) { create(:user) }
  let(:book) { create(:book) }

  before do
    sign_in user
  end

  describe 'POST #add_item' do
    it 'adds a book to the user cart' do
      post :add_item, params: { book_id: book.id, quantity: 1 }
      expect(response).to have_http_status(:ok)
      expect(user.cart.cart_items.first.quantity).to eq(1)
    end
  end

  describe 'POST #remove_item' do
    before do
      user.cart.cart_items.create(book: book, quantity: 1)
    end

    it 'removes a book from the cart' do
      delete :remove_item, params: { book_id: book.id }
      expect(response).to have_http_status(:ok)
      expect(user.cart.cart_items.count).to eq(0)
    end
  end

  describe 'POST #checkout' do
    before do
      user.cart.cart_items.create(book: book, quantity: 1)
    end

    it 'successfully processes the checkout' do
      post :checkout
      expect(response).to have_http_status(:ok)
      user.reload
      expect(user.cart.cart_items).to be_empty
      expect(JSON.parse(response.body)['message']).to eq('Payment successful')
    end
  end
end
